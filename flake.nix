{
  description = "fesite";
  inputs = {
    nixpkgs.url = github:nixos/nixpkgs/nixos-unstable;
    mozilla = { url = github:mozilla/nixpkgs-mozilla; flake = false; };
  };

  outputs = inputs@{ self, ... }:
    let
      rustOverlay = final: prev:
        let
          rustChannel = prev.rustChannelOf {
            channel = "nightly";
            date = "2021-09-27";
            sha256 = "sha256-TpTZHzBCNkFKB/VJvCCQuHcCeSzJO/iYz6L1ybh8lDc=";
          };
        in
        {
          inherit rustChannel;
          rustc = rustChannel.rust;
          cargo = rustChannel.rust;
        };

      pkgs = import inputs.nixpkgs {
        system = "aarch64-linux";
        overlays = [ (import "${inputs.mozilla}/rust-overlay.nix") rustOverlay ];
      };

      src = builtins.path {
        path = ./.;
        name = "fesite";
      };

      config = pkgs.stdenv.mkDerivation {
        pname = "fesite-config";
        version = "HEAD";
        buildInputs = [ pkgs.dhall ];
        src = ./.;

        phases = "installPhase";

        installPhase = ''
          cd ${src}
          dhall resolve < ${src}/config.dhall >> $out
        '';
      };

      fesite = pkgs.rustPlatform.buildRustPackage rec {
        pname = "fesite";
        verstion = "1.0.0";
        name = "fesite";
        cargoSha256 = "sha256-7uPSlNAWb4IYVoWjidyxXF7IFJyrtrTE0tX8iv6TBGM=";
        root = ./.;
        src = ./.;
        nativeBuildInputs = [ pkgs.pkg-config ];
        buildInputs = [ pkgs.openssl pkgs.git ];
        # doCheck = true;
        # remapPathPrefix = true;
      };

    in
    {
      packages.aarch64-linux.fesite =
        pkgs.stdenv.mkDerivation {
          inherit (fesite) name;
          inherit src;
          # name = "fesite";
          # src = ./.;
          phases = "installPhase";

          installPhase = ''
             mkdir -p $out $out/bin

             cp -rf ${config} $out/config.dhall
             cp -rf $src/blog $out/blog
             cp -rf $src/css $out/css
             cp -rf $src/gallery $out/gallery
             cp -rf $src/signalboost.dhall $out/signalboost.dhall
             cp -rf $src/static $out/static
             cp -rf $src/talks $out/talks
             cp -rf ${fesite}/bin/fesite $out/bin/fesite
           '';
        };
    };
}
