{ sources ? import ./sources.nix }:

let
  pkgs =
    import sources.nixpkgs { overlays = [ (import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz)) ]; };
  channel = "nightly";
  date = "2021-09-27";
  targets = [ ];
  chan = pkgs.rustChannelOfTargets channel date targets;
in chan
