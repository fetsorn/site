{ pkgs, stdenv, cmake, ... }:

stdenv.mkDerivation {
  pname = "hello-repeater";
  version = "1.0.0";
  src = pkgs.fetchgit {
    url = "https://github.com/breakds/flake-example-hello-repeater.git";
    rev = "c++-code-alone";
    sha256 = "sha256-/3tT3jBmWLaENcBRQhi2o3DHbBp2yiYsq2HMD/OYXNU=";
  };

  nativeBuildInputs = [
    cmake
  ];
}
